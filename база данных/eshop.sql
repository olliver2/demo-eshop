-- MySQL dump 10.15  Distrib 10.0.22-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: u887897590_eshop
-- ------------------------------------------------------
-- Server version	10.0.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(400) NOT NULL,
  `feature` varchar(400) NOT NULL,
  `price` varchar(50) NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (1,'mobile','Смартфон Apple iPhone 4','iPhone 4 получил 3,5-дюймовый дисплей с разрешением 960 x 640 пикселей. Толщина устройства всего 9,3 мм. Передняя и задняя стороны аппарата обе плоские, выполнены из стекла, торцевая окантовка – стальная.','Тип:смартфон\r\nОперационная система:iOS\r\nВерсия:4\r\nПроцессор:Apple A4\r\nЧастота:1000 МГц\r\nФлэш-память:32768 МБ','500','iphone4.png'),(2,'mobile','Cмартфон Apple iPhone5','iPhone 5 оснащен 4-дюймовым дисплеем Retina, с разрешением 1136 x 640 пикселей. Разрешающая способность нового экрана составляет прежние 326 ppi. Кроме того, Apple на 44% улучшила цветопередачу экрана.','Тип:смартфон\r\nОперационная система:iOS\r\nВерсия:6\r\nПроцессор:Apple A6\r\nОперативная память:1024 МБ\r\nФлэш-память:32768 МБ','300','iphone2.png'),(3,'mobile','Смартфон Sony Xperia Z3','В Xperia Z3 сочетаются уникальный дизайн и оригинальные технические решения. Смартфон наделен алюминиевой рамкой со скругленными углами и имеет толщину всего 7,3 мм. ','Тип:смартфон\r\nОперационная система:Android\r\nВерсия:4.4.4\r\nПроцессор:Qualcomm Snapdragon 801\r\nЧастота:2500 МГц\r\nОперативная память:3072 МБ\r\nФлэш-память:16384 МБ\r\nПоддержка флеш-карт:microSD (Transflash)','150','sonyxperiaz3.png'),(4,'notebook','Ноутбук Apple Macbook Air','Несомненно, MacBook Air невероятно тонкий и лёгкий. Но в Apple также позаботились о том, чтобы он был мощным, производительным, надёжным и удобным в использовании.','Тип:ноутбук\r\nЦентральный процессор:Intel Core 2 Duo\r\nЧастота процессора:1.8 ГГц\r\nТип оперативной памяти:DDR2-667 (PC2-5200)\r\nОбъем оперативной памяти:2048 МБ\r\nОбъем накопителя:64 ГБ\r\nДиагональ экрана:13.3\r\nРазрешение и формат экрана:1280 x 800 WXGA 16:10','500','notebook2.png'),(5,'notebook','Ноутбук ASUS G750JH-CV015H','Эту модель отличает агрессивный дизайн в стиле истребителя-невидимки, причем сужающаяся форма корпуса способствует комфортному положению рук во время длительных геймерских сессий. ','Тип:ноутбук\r\nОперационная система:Microsoft Windows 8\r\nЦентральный процессор:Intel Core i7\r\nЧастота процессора:2.4 ГГц\r\nТип оперативной памяти:DDR3-1600 (PC3-12800) 2x800МГц\r\nОбъем оперативной памяти:32768 МБ\r\nОбъем накопителя:1256 ГБ\r\nДиагональ экрана:17.3 \"\r\nРазрешение и формат экрана:1920 x 1080 WUXGA 16:9\r\nГрафический процессор:nVidia GeForce GTX 780M\r\nРазрешение камеры:2 Мпикс.','300','ASUSG750JH-CV015H.png'),(6,'notebook','Ноутбук MSI GT70 2PE-1217RU','MSI GT70 Dominator Pro - игровой лаптоп с видеокартой последней модели NVIDIA GeForce GTX880M. Уникальный рассчитанный на геймеров дизайн, рекордная мощность и возможность полного погружения.','Тип:ноутбук\r\nОперационная система:Microsoft Windows 8.1\r\nЦентральный процессор:Intel Core i7\r\nЧастота процессора:3 ГГц\r\nТип оперативной памяти:DDR3-1600 (PC3-12800) 2x800МГц\r\nОбъем оперативной памяти:32768 МБ\r\nОбъем накопителя:1384 ГБ\r\nДиагональ экрана:17.3\r\nРазрешение и формат экрана:1920 x 1080 WUXGA 16:9\r\nГрафический процессор:Intel GMA HD ','350','MSIGT702PE-1217RU.png'),(7,'tablet','Планшет Apple iPad','Apple iPad - планшетный компьютер с сенсорным экраном диагональю 9,7 дюйма. Интерфейс iPad унаследован с существующих продуктов Apple и легок в управлении так же, как интерфейс iPhone','Тип:планшетный ПК\r\nЦентральный процессор:Apple A4\r\nЧастота процессора:1 ГГц\r\nОбъем накопителя:64 ГБ\r\nДиагональ экрана:9.7\r\nРазрешение и формат экрана:1024 x 768 XGA 4:3','200','Ipad2.png'),(8,'tablet','Планшет Sony Xperia Tablet Z2','Sony Xperia Tablet Z2 предназначен для комфортного просмотра видео, чтения электронных книг и работе в интернете в дороге или в помещении.','Тип:планшетный ПК\r\nОперационная система:Android 4.4\r\nЦентральный процессор:Qualcomm Snapdragon\r\nЧастота процессора:2.3 ГГц\r\nОбъем оперативной памяти:3072 МБ\r\nОбъем накопителя:16 ГБ\r\nУстройство чтения карт памяти:microSD\r\nДиагональ экрана:10.1\r\nРазрешение и формат экрана:1920 x 1200 WUXGA 16:10','150','SonyXperiaTabletZ2.png'),(20,'mobile','qwdqwwwwwwwwwwwwwww','wwwwwwwwwwwwwwwwwwwww','wwwwwwwwwwwwwwwwwwwwwwwwww','wwwwww','dart.ico');
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `storeid` varchar(20) NOT NULL,
  `count` varchar(20) NOT NULL,
  `totalprice` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'Sasha','0502553991','olliver2@mail.ru','8,1','12,3','2400'),(61,'Яна Дюминаа','380950492909','djumina1992@gmail.com','1','1','500'),(60,'Александр Дубовицкий','380502553991','tale07@mail.ru','2','12','3600'),(57,'sashka kakashka','380987866539','sapoy_734@mail.ru','5','2','600'),(58,'Александр Дубовицкий','380502553991','tale07@mail.ru','5,12','12,21','9900'),(59,'акпиапиапипаиваип','380938491830','vitaly_druzhko@mail.ru','12,1','1,1','800');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-20  7:25:21
