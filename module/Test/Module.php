<?php

namespace Test;

 use Zend\Mvc\ModuleRouteListener;
 use Zend\Mvc\MvcEvent;
 use Test\Model\Goods;
 use Test\Model\GoodsTable;
use Test\Model\Orders;
 use Test\Model\OrdersTable;
 use Zend\Db\ResultSet\ResultSet;
 use Zend\Db\TableGateway\TableGateway;

class Module
{

    public function getServiceConfig()
    {
        return array(
        'factories'=>array(
            'GoodsTable'=>function($sm){
                $tableGateway=$sm->get('GoodsTableGateway');
                $goodsTable=new GoodsTable($tableGateway);
                return $goodsTable;
            },
              'GoodsTableGateway'=>function($sm){
            $dbadapter=$sm->get("Zend\Db\Adapter\Adapter");
            $resultset=new ResultSet();
            $resultset->setArrayObjectPrototype(new Goods());
            return new TableGateway('goods',$dbadapter,null,$resultset);
            },
            'OrdersTable'=>function($sm){
                $tableGateway=$sm->get('OrdersTableGateway');
                $ordersTable=new OrdersTable($tableGateway);
                return $ordersTable;
            },
            'OrdersTableGateway'=>function($sm){
            $dbadapter=$sm->get("Zend\Db\Adapter\Adapter");
            $resultset=new ResultSet();
            $resultset->setArrayObjectPrototype(new Orders());
            return new TableGateway('orders',$dbadapter,null,$resultset);
            },

            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
