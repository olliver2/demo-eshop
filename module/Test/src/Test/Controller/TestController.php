<?php

namespace Test\Controller;

session_start();
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Test\Model\Orders;
use Test\Model\Goods;

class TestController extends AbstractActionController
{
    public function indexAction()
    {
    	return array();
    }
    
    public function mobilesAction()
    {
        //Cоздаем экземпляр класса GoodsTable.
        $tableGoods=$this->getServiceLocator()->get('GoodsTable');
        //Вызывает метод fetchCategory который выбирает товары из бд по категории товара.
        $mobiles=$tableGoods->fetchCategory('mobile');
        //Кидаем в представление все товары с категорией 'mobile'.
        return array('mobiles'=>$mobiles);
    }
    
    public function tabletAction()
    {
        //Cоздаем экземпляр класса GoodsTable.
        $tableGoods=$this->getServiceLocator()->get('GoodsTable');
        //Вызывает метод fetchCategory который выбирает товары из бд по категории товара.
        $tablets=$tableGoods->fetchCategory('tablet');
        //Кидаем в представление все товары с категорией 'tablet'.
        return array('tablet'=>$tablets);    
    }
    
    public function notebookAction()
    {
        //Cоздаем экземпляр класса GoodsTable.
        $tableGoods=$this->getServiceLocator()->get('GoodsTable');
        //Вызывает метод fetchCategory который выбирает товары из бд по категории товара.
        $notebooks=$tableGoods->fetchCategory('notebook');
        //Кидаем в представление все товары с категорией 'notebook'.
        return array('notebook'=>$notebooks);
    }
    
    //Этот метод выводит полную информацию о выбраном товаре по его id из бд.
    public function infoAction()
    {
        //Получаем id.
        $id=(int)$this->params()->fromRoute('id');
        //Cоздаем экземпляр класса GoodsTable.
        $goodstable=$this->getServiceLocator()->get('GoodsTable');
        //Вызываем метод getGoods и кидаем в него $id,метод getGoods выбирает товар по id.
        $goods=$goodstable->getGoods($id);
        //Кидаем всю информацию о товаре в массив,после чего кидаем его в представление.
        $data[]=$goods;
        return array('id'=>$id,'good'=>$data);
    }
    
    //Метод который сохраняет выбранный товар покупателя в Сессию.
    public function savebasketAction()
    {
        //Создаем Сессию,в которой будем хранить id товара и его количество.
        $_SESSION['goods'];
        //Принимаем id из URL.
        $id=$this->params()->fromRoute('id');
        //Проверяем существует ли id.
        if(isset($id)){
            if(isset($_SESSION['goods'][$id])){
            //Eсли id cуществует проверяем ,cуществует ли ключ с таким id в Сессии ,если да ,то увеличиваем его значение на еденицу. 
            $_SESSION['goods'][$id]++;
        }
        else{
            //Если в сессии такого id нету ,создаем ключ c пришедшим id и задаем ему значение 1.
            $_SESSION['goods'][$id]=1;    
        }
    }
    //Делаем перенаправление  в представление basket ,где будет выводится все товары выбранные покупателем.
    return $this->redirect()->toRoute('home',array('action'=>'basket'));
    }
    
    //Метод вывода выбраного товара покупателем ,а так же его пересчета.
    public function basketAction()
    {
    //Проверяем если данные в Сессии.
    if(!empty($_SESSION['goods'])){
    //Если есть ,получаем все ключи в которых находится id товара и кидаем их в переменную $ids.
        $ids=array_keys($_SESSION['goods']);
    }else{
    //Eсли в сессии нету данных делаем перенаправление на главную страницу.
        return $this->redirect()->toRoute('home');
    }
    
    //Получаем все данные пришедшие из формы.
    $request=$this->getRequest();
    //Делаем проверку на отправку данных методом POST.
    if($request->isPost()){
        //В переменную id сохраняем данные из $_POST['id']
        $id=$request->getPost('id');
        //В переменную count сохраняем данные из $_POST['count']
        $count=$request->getPost('count');
        /*Прогоняем через цикл $_POST['id'],$_POST['count'] и $_SESSION['goods']
        В $_POST['id'] мы получаем id товара у которого надо изменить количество,а в
        $_POST['count'] мы получаем новое количество.
        */
        foreach ($_SESSION['goods'] as $skey => $svalue) {
            foreach ($id as $postidkey => $postidvalue) {
                    foreach ($count as $postcoukey => $postcouvalue) {
                        /*Ecли ключ из $_SESSION['goods'] и ключ из $_POST['id'] совпадают (мы сравнимаем id товара)
                        и ключи $_POST['id'] и $_POST['count'] совпадают то мы меняем старое кол-во товара на новое.
                        */
                        if($skey == $postidvalue and $postidkey == $postcoukey){
                            $_SESSION['goods'][$skey]=$postcouvalue;
                        }
                    }
                }
            }
        }
    //Создаем экземпляр класса GoodTable.
    $goodstable=$this->getServiceLocator()->get('GoodsTable');
     //Вызываем метод fetchGoods,в который выбирает все товары по id из бд.
    $goodsinbasket=$goodstable->fetchGoods($ids);
    //Передаем в представление basket информацию о выбранных товарах и Сессию с данными.
    return array('basket'=>$goodsinbasket,'session'=>$_SESSION['goods']);
    }
    
    //Метод оформление заказа.
    public function orderAction()
    {
        //Получаем все выбранные товары по id из Сессии.
        $ids=array_keys($_SESSION['goods']);

        if(empty($_SESSION['goods'])){
            return $this->redirect()->toRoute('home');
        }
        //Создаем экземпляр класса GoodTable.
        $goodstable=$this->getServiceLocator()->get('GoodsTable');
        //Вызываем метод fetchGoods, который выбирает все товары по id из бд.
        $goodsinbasket=$goodstable->fetchGoods($ids);
        
        //Cоздаем валидатор для проверки правильности email адресса.
        $emailvalid = new \Zend\Validator\EmailAddress();
        //Задаем ему сообщение об ошибку в случаем если email не валиден.
        $emailvalid->setMessages(array(\Zend\Validator\EmailAddress::INVALID_FORMAT => 'Email введен не верно!'));
        //Cоздаем валидатор для проверки правильности моб.телефона.
        $phonevalid=new \Zend\Validator\Regex(array('pattern' => '/^[+3]\d{2}\d{3}\d{3}\d{3}$/'));

        //Получаем все данные пришедшие из формы.
        $request=$this->getRequest();
        //Делаем проверку на отправку данных методом POST.
        if($request->isPost()){
            //Если данные пришли методом POST ,делаем проверку эмейла и телефона.
            if($emailvalid->isValid($request->getPost('email')) and $phonevalid->isValid($request->getPost('phone'))){
            //Если данные прошли проверку,cоздаем экземпляр класса Orders.
            $order=new Orders();
            //Вызывает метод еxchangeArray куда кидаем данных пришедшие из формы методом POST.
            $order->exchangeArray($request->getPost());
            //Создаем экземпляр класса OrdersTable.
            $orderstable=$this->getServiceLocator()->get('OrdersTable');
            //После чего вызываем метод saveOrder и кидаем туда данные из exchangeArray,данных сохраняются в таблицу orders.
            $orderstable->saveOrders($order);

            $this->redirect()->toRoute('home',array('action'=>'complete'));
            }else{
                //Ecли данные не валидны выводим сообщение об ошибке.
                foreach ($emailvalid->getMessages() as $message){
                $message;
                }
                $errophone="Неверный формат телефона!";
            }
        }
        //В предсталение передаем выбранные товары ,cессию с данными ,сообщение об ошибках
        return array('order'=>$goodsinbasket,'session'=>$_SESSION['goods'],'erromail'=>$message,'errorphone'=>$errophone);
    }

    //Метод удаляет из Сессии не нужные товар.
    public function deleteAction()
    {   //Принимаем id из URL.
        $id=(int)$this->params()->fromRoute('id');
        if($id){
            //Ecли id пришло удаляет его из Сессии.
            unset($_SESSION['goods'][$id]);
            $this->redirect()->toRoute('home',array('action'=>'basket'));
        }
    }
    
    //Метод удаляет товар из бд.
    public function deleteGoodsAction()
    {   //Принимаем id из URL.
        $id=(int)$this->params()->fromRoute('id');
        //Cоздаем экземпляр класса GoodsTable и вызываем метод deletGoods,который удаляет товар из бд.
        $this->getServiceLocator()->get('GoodsTable')->deleteGoods($id);
        return $this->redirect()->toRoute('home',array('action'=>'administration'));
    }

    //Meтод удаляет заказ из бд.
    public function deleteOrdersAction()
    {
        //Принимаем id из URL.
        $id=(int)$this->params()->fromRoute('id');
        //Cоздаем экземпляр класса GoodsTable и вызываем метод deletOrders,который удаляет заказ из бд.
        $this->getServiceLocator()->get('OrdersTable')->deleteOrders($id);
        return $this->redirect()->toRoute('home',array('action'=>'administration'));
    }
    
    //Метод выводящий все товары и заказы.
    public function administrationAction()
    {   //Cоздаем экземпляр класса OrdersTable и вызываем метод fetchAll,который выбирает все заказы из бд.
    	$orders=$this->getServiceLocator()->get('OrdersTable')->fetchAll();
        //Cоздаем экземпляр класса GoodsTable и вызываем метод fetchAll,который выбирает все товары из бд.
    	$goods=$this->getServiceLocator()->get('GoodsTable')->fetchAll();
        //Передаем все заказы и товары в представление.
        return array('order'=>$orders,'goods'=>$goods);
    }
   
    //Метод добавляет новый товар в бд.
    public function addAction()
    {
    
        //Получаем все данные из формы.
    	$request=$this->getRequest();
        //Делаем проверку на отправку данных методом POST.
    	if($request->isPost()){

            $uploaddir="/home/u887897590/public_html/img/";
            $uploadfile = $uploaddir . $_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
            if(isset($_FILES['image']['name'])){
               $request->getPost()->img=$_FILES['image']['name'];
            }
            //Если данные пришли методом POST сохраняем их в переменную $post.;
            $post=$request->getPost();
            //Cоздаем экземпляр класса Goods.
            $goods=new Goods();
            //Вызываем метод exchangeArray куда сохряняем все данные из формы.
            $goods->exchangeArray($post);
            //После чего вызываем метод saveGoods и кидаем туда данные из exchangeArray,данных сохраняются в таблицу goods.
            $goodstable=$this->getServiceLocator()->get('GoodsTable')->saveGoods($goods);

            return $this->redirect()->toRoute('home',array('action'=>'administration'));
        }
    	return array();
        
    }
    
    //Метод изменяет данные товара в бд.
    public function editAction()
    {
        //Принимаем id из URL.
    	$id=(int)$this->params()->fromRoute('id');
        //Получаем все данные из формы.
    	$request=$this->getRequest();
        //Делаем проверку на отправку данных методом POST.
    	if($request->isPost()){
            $uploaddir="/home/u887897590/public_html/img/";
            $uploadfile = $uploaddir . $_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
            if(isset($_FILES['image']['name'])){
               $request->getPost()->img=$_FILES['image']['name'];
            }
            //Если данные пришли методом POST сохраняем их в переменную $post.
    		$post=$request->getPost();
            //Cоздаем экземпляр класса Goods.
    		$goods=new Goods();
            //Вызываем метод exchangeArray куда сохряняем все данные из формы.
    		$goods->exchangeArray($post);
            //После чего вызываем метод saveGoods и кидаем туда данные из exchangeArray,данных сохраняются в таблицу goods.
    		$goodstable=$this->getServiceLocator()->get('GoodsTable')->saveGoods($goods);
    		return $this->redirect()->toRoute('home',array('action'=>'administration'));
    	}
        //Передаем в представление id товара ,и помещаем его в скрытый input.
    	return array('id'=>$id);
    }

    public function completeAction(){
        return array();
    }
}