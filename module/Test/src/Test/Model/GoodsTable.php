<?php

	namespace Test\Model;
	use Zend\Db\ResultSet\ResultSet;
 	use Zend\Db\TableGateway\TableGateway;
 	use Zend\Db\Adapter\Adapter;
 	use Zend\Db\Sql\Select;

 class GoodsTable
{ 
	public $tableGateway;

	public function  __construct( TableGateway $tableGateway)
    {
		$this->tableGateway=$tableGateway;
	}
    
    public function fetchAll()
    {
        $result=$this->tableGateway->select(function(Select $select){
            $select->order('id DESC');
        });
        return $result;
    }
	
    //Метод выбирающий товары по категории.
    public function fetchCategory($someCondition) 
    {
        $resultSet = $this->tableGateway->select(function (Select $select) use ($someCondition) { 
    	$select->columns(array('id','title','description','feature','price','img'));
    	$select->where(array('category'=>$someCondition));
    });

    return $resultSet;
    }
    
    //Метод выбирающий товары по id.
    public function fetchGoods($someCondition) 
    {
        $resultSet = $this->tableGateway->select(function (Select $select) use ($someCondition) {
        //Выбираем колонки по id из Сессии.
        $select->columns(array('id','category','title','description','feature','price','img'));
        $select->where->in('id',$someCondition);
        });

        return $resultSet;
    }
	
    public function getGoods($id)
    {
		$id=(int)$id;
		$rowset=$this->tableGateway->select(array('id'=>$id));
		$row=$rowset->current();
		if (!$row) {
             
            throw new \Exception("Could not find row $category");
        }
        
        return $row;
	}
	
    public function getCategory($category)
    {
		$rowset=$this->tableGateway->select(array('category'=>$category));
		$row=$rowset->current();
		if (!$row) {
            throw new \Exception("Could not find row $category");
        }
         
        return $row;
	}
	
    public function deleteGoods($id)
    {
		$id = (int)$id;
		$this->tableGateway->delete(array('id'=>$id));
	}
    
    public function saveGoods(Goods $goods)
    {
        //Создаем фильтр для обрезание html тегов в данных.
        $filtettags= new \Zend\Filter\StripTags();
        //Создаем фильтр для обрезание пробелов в конце и в начале строки.
        $filtertrim= new \Zend\Filter\StringTrim();
        //Прогоняем все данные через фильтр.
        $data = array(
            'id' => $goods->id,
            'category'  => $filtertrim->filter($filtettags->filter($goods->category)),
            'title'  => $filtertrim->filter($filtettags->filter($goods->title)),
            'description'  => $filtertrim->filter($filtettags->filter($goods->description)),
            'feature'  => $filtertrim->filter($filtettags->filter($goods->feature)),
            'price'  => $filtertrim->filter($filtettags->filter($goods->price)),
            'img'  => $filtertrim->filter($filtettags->filter($goods->img)),
        );

        $id = (int) $goods->id;
        //Если товара с таким id нету,добавляем новый товар.
        if ($id==0) {
            
            $this->tableGateway->insert($data);
        
        }else{
            //Если товара с таким id eсть,обновляем товар.
            $this->tableGateway->update(array(
            "title"=>$goods->title,
            "category"=>$goods->category,
            "price"=>$goods->price,
            "img"=>$goods->img,),array("id"=>$id));
        }
    }
}
