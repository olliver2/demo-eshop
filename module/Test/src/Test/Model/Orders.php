<?php

namespace Test\Model;

class Orders
{ 
    public $id;
    public $name;
    public $phone;
    public $email;
    public $storeid;
    public $count;
    public $totalprice;

    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id']))     ? $data['id']     : null;
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->phone  = (isset($data['phone']))  ? $data['phone']  : null;
        $this->email  = (isset($data['email']))  ? $data['email']  : null;
        $this->storeid  = (isset($data['storeid']))  ? $data['storeid']  : null;
        $this->count  = (isset($data['count']))  ? $data['count']  : null;
        $this->totalprice  = (isset($data['totalprice']))  ? $data['totalprice']  : null;
    }

}
