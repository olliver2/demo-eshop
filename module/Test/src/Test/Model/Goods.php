<?php
namespace Test\Model;

class Goods
{ 
    public $id;
    public $category;
    public $title;
    public $description;
    public $feature;
    public $price;
    public $img;

    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id']))     ? $data['id']     : null;
        $this->category = (isset($data['category'])) ? $data['category'] : null;
        $this->title  = (isset($data['title']))  ? $data['title']  : null;
        $this->description  = (isset($data['description']))  ? $data['description']  : null;
        $this->feature  = (isset($data['feature']))  ? $data['feature']  : null;
        $this->price  = (isset($data['price']))  ? $data['price']  : null;
        $this->img  = (isset($data['img']))  ? $data['img']  : null;
    }
}