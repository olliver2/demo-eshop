<?php

namespace Test\Model;
	
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class OrdersTable
{ 
	public $tableGateway;

	
    public function  __construct( TableGateway $tableGateway)
    {
		$this->tableGateway=$tableGateway;
	}


	public function fetchAll() 
    {
        $resultSet = $this->tableGateway->select(function(Select $select){
            $select->order("id DESC");
        }); 

        return $resultSet;
    }

    public function saveOrders(Orders $orders)
    {   
        //Создаем фильтр для обрезание html тегов в данных.
        $filtettags= new \Zend\Filter\StripTags();
        //Создаем фильтр для обрезание пробелов в конце и в начале строки.
        $filtertrim= new \Zend\Filter\StringTrim();
        //Прогоняем все данные через фильтр.
        $data = array(
            'id' => $orders->id,
            'name'  => $filtertrim->filter($filtettags->filter($orders->name)),
            'phone'  => $filtertrim->filter($filtettags->filter($orders->phone)),
            'email'  => $filtertrim->filter($filtettags->filter($orders->email)),
            'storeid'  => $orders->storeid,
            'count'  => $orders->count,
            'totalprice'  => $orders->totalprice,
        );

        $id = (int) $orders->id;
            
        if ($id == 0) {
            
            $this->tableGateway->insert($data);
        
        }
    }
         
    public function deleteOrders($id)
    {
        
        $id = (int)$id;
        $this->tableGateway->delete(array('id'=>$id));
    }
}