$(function(){
		$('.arrow-next').on('click',function(){
			var currentSlide = $('.slide-active');
    		var nextSlide = currentSlide.next();

    		if(nextSlide.length === 0) {
      		nextSlide = $('.slide').first();
  }
    
    		currentSlide.hide().removeClass('slide-active');
    		nextSlide.show().addClass('slide-active');

		});
		 $('.arrow-prev').click(function() {
    	var currentSlide = $('.slide-active');
    	var prevSlide = currentSlide.prev();


    	if(prevSlide.length === 0) {
      	prevSlide = $('.slide').last();
    	}
    
    	currentSlide.hide().removeClass('slide-active');
    	prevSlide.show().addClass('slide-active');

  	});
	});